import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Recipe } from "../recipe.model";
import {Ingredient} from '../ingredient.model'

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styles: []
})
export class RecipeListComponent implements OnInit {
  @Output() recipeSelected = new EventEmitter<Recipe>();
  
  recipes: Recipe[] = [
    new Recipe(
      'Schnitzel', 
      'Sehr lecker', 
      'https://img.chefkoch-cdn.de/ck.de/rezepte/110/110923/946471-960x720-wiener-schnitzel.jpg',
      [
        new Ingredient('Pommes', 10),
        new Ingredient('Schnitzel', 1),
      ]
      ),
    
    new Recipe(
      'Salat', 
      'Sehr lecker', 
      'https://www.zugutfuerdietonne.de/typo3temp/_processed_/csm_Fotolia_Salat_e2dcdb3933.jpg',
      []
      ),
    ];
  
    selectedRecipe: Recipe;

  constructor() { }

  ngOnInit() {
  }

  onSelected(recipe: Recipe) {
    this.recipeSelected.emit(recipe);
  }

}
